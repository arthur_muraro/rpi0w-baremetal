#ifndef UART_H
#define UART_H

#define AUX_ENABLES 0x20215004

#define AUX_MU_IO_REG 0x20215040 // Mini Uart I/O Data | size : 8
#define AUX_MU_IER_REG 0x20215044 // Mini Uart Interrupt Enable | size : 8
#define AUX_MU_IIR_REG 0x20215048 // Mini Uart Interrupt Identify | size : 8
#define AUX_MU_LCR_REG 0x2021504C // Mini Uart Line Control | size : 8
#define AUX_MU_MCR_REG 0x20215050 // Mini Uart Modem Control | size : 8
#define AUX_MU_LSR_REG 0x20215054 // Mini Uart Line Status | size : 8
#define AUX_MU_MSR_REG 0x20215058 // Mini Uart Modem Status | size : 8
#define AUX_MU_SCRATCH 0x2021505C // Mini Uart Scratch | size : 8
#define AUX_MU_CNTL_REG 0x20215060 // Mini Uart Extra Control | size : 8
#define AUX_MU_STAT_REG 0x20215064 // Mini Uart Extra Status 3 | size : 2
#define AUX_MU_BAUD_REG 0x20215068 // Mini Uart Baudrate | size : 16

#endif // UART_H
