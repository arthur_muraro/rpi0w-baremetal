#include "gpio.h"
#include "utils.c"

int get_gpio_fsel_register(uint pin_number)
{
    // retrieve tens of gpio pin
    uint tens = pin_number / 10;
    return GPFSEL_BASE_ADDRESS + tens * 4;
}

void gpio_clear(uint pin_number)
{
    // retrieves units of gpio pin
    uint units = pin_number % 10;
    // select the right function register for the pin
    int gpfsel = read((void*)get_gpio_fsel_register(pin_number));
    // clear the potential existants parameters for the pin by righting zeroes
	gpfsel &= ~(7 << (units*3));
    write((void*)get_gpio_fsel_register(pin_number),gpfsel);
}

void gpio_on(uint pin_number)
{
    // retrieves units of gpio pin
    uint units = pin_number % 10;
    // select the right function register for the pin
    int gpfsel = read((void*)get_gpio_fsel_register(pin_number));
    // clear the potential existants parameters for the pin by righting zeroes
	gpfsel &= ~(7 << (units*3));
    // sets the pin in output mode
    gpfsel |= (1<<units*3);
    // get the register address reponsible for turning ON pin the pin
    uint gpfset = GPFSEL_BASE_ADDRESS + 0x1c + (pin_number / 32 * 4);

    // write the registers to set everything
    write((void*)get_gpio_fsel_register(pin_number),gpfsel);
    write((void*)gpfset,1<<pin_number);
}

void gpio_off(uint pin_number)
{    // get the register address reponsible for turning OFF pin the pin
    uint gpfclr = GPFSEL_BASE_ADDRESS + 0x28 + (pin_number / 32 * 4);
    write((void*)gpfclr,1<<pin_number);
}

void gpio_set_alt_function(uint pin_number, uint func_number)
{
    // retrieves units of gpio pin
    uint units = pin_number % 10;
    // select the right function register for the pin
    int gpfsel = read((void*)get_gpio_fsel_register(pin_number));
    // clear the potential existants parameters for the pin
    gpio_clear(pin_number);

    // sets the pin to specified alt function
    gpfsel |= (func_number << units*3);
    // write the register
    write((void*)get_gpio_fsel_register(pin_number),gpfsel);
}

/* 
    the state must be between 0 and 2
    00 = Off – disable pull-up/down
    01 = Enable Pull Down control
    10 = Enable Pull Up control
    11 = Reserved
*/
void gpio_set_pud(uint pin_number, uint state)
{
    if (!(state >=0 &&  state <= 2)) {
        return;
    }
    uint GPPUDCLKn = GPPUDCLK_BASE_ADDRESS + (pin_number / 32 * 4);

    write((void *)GPPUD,state);
    dummy(150);
    write((void *)GPPUDCLKn,1<<pin_number);
    dummy(150);
    write((void *)GPPUDCLKn,0);
}
