#ifndef GPIO_H
#define GPIO_H

#define GPFSEL_BASE_ADDRESS 0x20200000
#define GPPUD 0x20200094 // GPIO Pin Pull-up/down Enable
#define GPPUDCLK_BASE_ADDRESS 0x20200098

#define GPIO_FSEL_ALT0 0x04   // alternate function 0 0b100
#define GPIO_FSEL_ALT1 0x05   // alternate function 1 0b101
#define GPIO_FSEL_ALT2 0x06   // alternate function 2 0b110 
#define GPIO_FSEL_ALT3 0x07   // alternate function 3 0b111
#define GPIO_FSEL_ALT4 0x03   // alternate function 4 0b011
#define GPIO_FSEL_ALT5 0x02   // alternate function 5 0b010

#endif // GPIO_H