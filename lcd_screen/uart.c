#include "uart.h"
#include "gpio.c"

void mini_uart_init()
{
    // We first select the alternate function 5 for for GPIO 14 and 15
    // which are responsible for RXD and TXD on uart protocol
    gpio_set_alt_function(14, GPIO_FSEL_ALT5);
    gpio_set_alt_function(15, GPIO_FSEL_ALT5);

    // We then need to set thoses pins in "neither" state (as opposed to Pull-up/Down)
    gpio_set_pud(14, 0);
    gpio_set_pud(15, 0);

    // Now that the "physical" linking is done, we need to enable mini uart protocol
    // cf : page 8 of bcm2835 datasheet
    write((void*)AUX_ENABLES,1);
    write((void*)AUX_MU_CNTL_REG,0);
    write((void*)AUX_MU_IER_REG,0);
    write((void*)AUX_MU_LCR_REG,3);
    write((void*)AUX_MU_MCR_REG,0);
    write((void*)AUX_MU_BAUD_REG,270);
    write((void*)AUX_MU_CNTL_REG,3);
}

void mini_uart_send(char c)
{
    while(1) {
        if(read((void*)AUX_MU_LSR_REG)&0x20) 
            break;
    }
    write((void*)AUX_MU_IO_REG,c);
}

char mini_uart_recv(void)
{
    while(1) {
        if(read((void*)AUX_MU_LSR_REG)&0x01) 
            break;
    }
    return(read((void*)AUX_MU_IO_REG)&0xFF);
}

void mini_uart_send_string(char* string)
{
    for (int c=0; string[c] != "\0";c++) {
        mini_uart_send(string[c]);
    }
}
