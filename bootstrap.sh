#!/bin/bash
# 18/04/2024
# Arthur Muraro / Kaddate

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

if [ ! "$#" -eq 2 ]; then
  echo "usage : ./bootstrap.sh <device> <build_dir>"
  exit
fi

build_dir=$2
target_device=$1

mkdir sources 2>/dev/null
if [[ ! -f "sources/start.elf" ]]; then
    printf "Downloading bootloader\n"
    wget https://github.com/raspberrypi/firmware/raw/master/boot/start.elf -O sources/start.elf
fi
# if [[ ! -f "sources/kernel7.img" ]]; then
#     printf "Downloading kernel\n"
#     wget https://github.com/raspberrypi/firmware/raw/master/boot/kernel7.img -O sources/kernel7.img
# fi
if [[ ! -f "sources/bootcode.bin" ]]; then
    printf "Downloading bootcode\n"
    wget https://github.com/raspberrypi/firmware/raw/master/boot/bootcode.bin -O sources/bootcode.bin
fi

printf "compiling the program into kernel.img\n"
cd $build_dir
make clean
make all
cd ..

mkdir /tmp/sd_drive 2>/dev/null
printf "mounting device\n"
mount /dev/mmcblk0 /tmp/sd_drive
printf "copying sources into the sd drive\n"
cp sources/* /tmp/sd_drive/
# cp sources/kernel7.img /tmp/sd_drive/

printf "Unmounting the drive\n"
umount /tmp/sd_drive

printf "Time to run it !\n"