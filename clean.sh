#!/bin/bash
# 29/04/2024
# Arthur Muraro / Kaddate

if [ "$EUID" -ne 0 ]
  then echo "Please run as root"
  exit
fi

if [ ! "$#" -eq 1 ]; then
  echo "usage : ./clean.sh <build_dir>"
  exit
fi

build_dir=$1

printf "cleaning the building directory\n"
cd $build_dir
make clean
cd ..