typedef unsigned int uint;

// writes given value to the destination address
void write(void *dst, uint val)
{
    uint* dst_u = (uint*)dst;
    *dst_u = val;
    return;
}

// returns value of given pointer
uint read(void *src)
{
    uint* src_u = (uint*)src;
    return *src_u;
}

#pragma GCC push_options
#pragma GCC optimize("O0")
uint dummy(uint val)
{
    return val;
}
#pragma GCC pop_options