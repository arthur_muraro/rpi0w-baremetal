#include "gpio.c"

int main ( void )
{
    int ra;

    while(1)
    {
        // This for loop does nothing but kind of acts as a "sleep".
        for(ra=0;ra<0x200000;ra++) dummy(ra);
        // turn ON the LED by setting output of LED 21 to 1 
        gpio_on(21);

        for(ra=0;ra<0x200000;ra++) dummy(ra);
        // turn OFF the LED by clearing output of LED 21 to 1 
        gpio_off(21);
    }

    return 0;
}