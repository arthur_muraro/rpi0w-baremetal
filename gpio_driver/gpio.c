#include "utils.c"

#define GPIO_BASE_ADDRESS 0x20200000

int get_gpio_fsel_register(uint pin_number)
{
    // retrieve tens of gpio pin
    uint tens = pin_number / 10;
    return GPIO_BASE_ADDRESS + tens * 4;
}

void gpio_on(uint pin_number)
{
    // retrieves units of gpio pin
    uint units = pin_number % 10;
    // select the right function register for the pin
    int gpfsel = read((void*)get_gpio_fsel_register(pin_number));
    // clear the potential existants parameters for the pin by righting zeroes
	gpfsel &= ~(7 << (units*3));
    // sets the pin in output mode
    gpfsel |= (1<<units*3);
    // get the register address reponsible for turning ON pin the pin
    uint gpfset = GPIO_BASE_ADDRESS + 0x1c + (pin_number / 32 * 4);

    // write the registers to set everything
    write((void*)get_gpio_fsel_register(pin_number),gpfsel);
    write((void*)gpfset,1<<pin_number);
}

void gpio_off(uint pin_number)
{    // get the register address reponsible for turning OFF pin the pin
    uint gpfclr = GPIO_BASE_ADDRESS + 0x28 + (pin_number / 32 * 4);
    write((void*)gpfclr,1<<pin_number);
}