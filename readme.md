# Kernel from scratch on rpi 0 w

## resources

- https://github.com/raspberrypi/firmware/tree/master/boot
- https://github.com/raspberrypi/linux
- https://www.valvers.com/open-software/raspberry-pi/bare-metal-programming-in-c-part-1/
- https://github.com/dbhicks/pi-zero-bare-metal
- https://github.com/dwelch67/raspberrypi-zero

## sources

- get bootloader : https://github.com/raspberrypi/firmware/raw/master/boot/start.elf
<!-- - get kernel : https://github.com/raspberrypi/firmware/raw/master/boot/kernel7.img -->
- get bootcode : https://github.com/raspberrypi/firmware/raw/master/boot/bootcode.bin

## Where the f am I ?

You are accompanying me on an adventure to create a kernel (or something close to it) from scratch on a Raspberry Pi 0 W. The objective is to gain practical knowledge of kernel architecture, moving beyond mere theory. Additionally, I aim to delve into hardware aspects since I'll need to work with external modules to implement I/O functionality.

All my kernels step by step :
- [Basic program that blinks a led ¯\\_\(ツ\)\_/¯](./blink_gpio_led_21/)
- [Yet another program that blinks a led ¯\\_\(ツ\)\_/¯ But with a driver for GPIOs now !](./gpio_driver/) Since i'll need drivers to handle ICC,UART,RS-232
- [Kernel implementing UART so I can have some output during runtime via an LCD screen](./lcd_screen/). Initially, I intended to handle interruptions (cf: [button_led_gpio](.button_led_gpio/)), but it's a bit challenging to implement without debugging capabilities. So, I focused first on handling outputs. After finishing the UART implementation, I realized that my screen only handles parallel communications (－‸ლ). So, I bought a parallel to ICC converter. Now, I'm on my way to implement ICC.
- [button_led_gpio]("./lcd_screen/") The goal is to implement interrupts for my kernel. So when i press a button, it turns ON/OFF a LED. Both modules being plugged to the RPI0 via it's GPIO PINS. This involves a lot of very interesting process :

#### Interrupts workflow

Refering to sections D1.10 to D1.10.2 of `ARM Architecture Reference Manual ARMv8, for ARMv8-A architecture profile` (page 1874-1875) :

```
When the PE takes an exception to an Exception level that is using AArch64, execution is forced to an address that is the exception vector for the exception. The exception vector exists in a vector table at the Exception level the exception is taken to.

Each Exception level has an associated Vector Base Address Register (VBAR), that defines the exception base address for the table at that Exception level. 

For exceptions taken to AArch64 state, the vector table provides the following information:
• Whether the exception is one of the following:

— Synchronous exception.
— SError.
— IRQ.
— FIQ.
```

For our project, we will only handle IRQ interruptions

We can base our vector table from this :

![alt text](images/image.png)

#### How to do it ?

First, I want to get interrupts from exception level 1 (which appears to be the actual execution level of the linux kernel).

1. GET from EL3 to EL1

Then define the exception vector table that stores the address of all routines for all interrtuptions (4x4 interruptions).

2. Define the exception vector table
3. Defines the handlers for each exception vectors

Implement the logic that stores EL0 registers to jump to EL1, do the operation requested and then switch back registers for EL0

4. implement kernel_entry that saves registers before interruption handling
5. implement kernel_exit to revert the preceding step after interruption handling
6. set the VBAR register
7. accept irqs (unmask)

Add Output and input (as the button)

8. implement a driver for GPIO pins
9. connect the hardware

Code !

10. implement project logic

## Kernel 1 & 2 (I know, it's very impressive -_-' )

<figure class="video_container">
  <iframe src="images/kernel_1_2.mp4" frameborder="0" allowfullscreen="true"> 
</iframe>
</figure>

Link to video if not shown : [link](images/kernel_1_2.mp4)

## Kernel 3 (Yup, the one with the screen with no protocol implementation....)

<img src="images/kernel_3.jpg" alt="Kernel 3" width="400"/>

<img src="images/ICC_converter.jpg" alt="ICC converter" width="400"/>
