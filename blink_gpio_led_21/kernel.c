// register address of first GPIO PIN 20 to 29
#define GPFSEL2 0x20200008
// register for LED ON
#define GPFSET0 0x2020001C
// register for LED OFF
#define GPFCLR0 0x20200028

typedef unsigned int uint;

#pragma GCC push_options
#pragma GCC optimize("O0")
uint dummy(uint val)
{
    return val;
}
#pragma GCC pop_options

// writes given value to the destination address
void write(void *dst, uint val)
{
    uint* dst_u = (uint*)dst;
    *dst_u = val;
    return;
}

// returns value of given pointer
uint read(void *src)
{
    uint* src_u = (uint*)src;
    return *src_u;
}

int main ( void )
{
    // Set the PIN 21 in output mod by adding 1 left shifted by 3 (8)
    // The arguments for pin 21 are located at 5-3 bits.
    // 0x1<<0x100 = 0x1000
    uint ra=read((void*)GPFSEL2);
    ra|=1<<3;
    write((void*)GPFSEL2,ra);

    while(1)
    {
        // This for loop does nothing but kind of acts as a "sleep".
        for(ra=0;ra<0x200000;ra++) dummy(ra);
        // turn ON the LED by setting output of LED 21 to 1 
        write((void*)GPFSET0,1<<21);

        for(ra=0;ra<0x200000;ra++) dummy(ra);
        // turn OFF the LED by clearing output of LED 21 to 1 
        write((void*)GPFCLR0,1<<21);
    }

    return(0);
}