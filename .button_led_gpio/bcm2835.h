#ifndef BCM2835_H
#define BCM2835_H

// register address of GPIO PIN 20 to 29
#define GPFSEL2 0x20200008
// register for LED ON
#define GPFSET0 0x2020001C
// register for LED OFF
#define GPFCLR0 0x20200028

#endif /* BCM2835_H */
