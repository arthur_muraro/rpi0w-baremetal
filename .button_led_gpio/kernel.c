
// program architecture
// - BCM2835 headers
// - driver for GPIO PINS
// - Interrupts implementatoin
// - Kernel loop

// Exception vectors table
#define SYNCHRONOUS_INTERRUPT_EL1t
#define IRQ_INTERRUPT_EL1t
#define FIQ_INTERRUPT_EL1t
#define SERROR_INTERRUPT_EL1t

#define SYNCHRONOUS_INTERRUPT_EL1h
#define IRQ_INTERRUPT_EL1h
#define FIQ_INTERRUPT_EL1h
#define SERROR_INTERRUPT_EL1h

#define SYNCHRONOUS_INTERRUPT_EL0_AARCH64
#define IRQ_INTERRUPT_EL0_AARCH64
#define FIQ_INTERRUPT_EL0_AARCH64
#define SERROR_INTERRUPT_EL0_AARCH64

#define SYNCHRONOUS_INTERRUPT_EL0_AARCH32
#define IRQ_INTERRUPT_EL0_AARCH32
#define FIQ_INTERRUPT_EL0_AARCH32
#define SERROR_INTERRUPT_EL0_AARCH32

void handle_el1_irq() {

}

void handle_invalid_interrupt() {
    
}


int main(void)
{
    
}