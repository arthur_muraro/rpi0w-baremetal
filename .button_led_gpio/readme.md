# Button led GPIO

The goal to implement interrupts for my kernel. So when i press a button, it turns ON/OFF a LED. Both modules being plugged to the RPI0 via it's GPIO PINS.

# Interrupts workflow

Refering to sections D1.10 to D1.10.2 of `ARM Architecture Reference Manual ARMv8, for ARMv8-A architecture profile` (page 1874-1875) :

```
When the PE takes an exception to an Exception level that is using AArch64, execution is forced to an address that is the exception vector for the exception. The exception vector exists in a vector table at the Exception level the exception is taken to.

Each Exception level has an associated Vector Base Address Register (VBAR), that defines the exception base address for the table at that Exception level. 

For exceptions taken to AArch64 state, the vector table provides the following information:
• Whether the exception is one of the following:

— Synchronous exception.
— SError.
— IRQ.
— FIQ.
```

For our project, we will only handle IRQ interruptions

We can base our vector table from this :

![alt text](image.png)

# How to do it ?


First, I want to get interrupts from exception level 1 (which appears to be the actual execution level of the linux kernel).
1. GET from EL3 to EL1
2. Define the exception vector table
3. Defines the handlers for each exception vectors
4. implement kernel_entry that saves registers before interruption handling
5. implement kernel_exit to revert the preceding step after interruption handling
6. set the VBAR register
7. accept irqs (unmask)
8. implement a driver for GPIO pins
9. connect the hardware
10. implement project logic

## 1. GET from EL3 to EL1

